package uz.team.controller;


//Asadbek Xalimjonov 2/16/22 11:45 AM

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")

public class HomeController {


    @GetMapping()
    public String homePage() {
        return "static/user/index";
    }
}
