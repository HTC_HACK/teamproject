package uz.team.controller.auth;



//Asadbek Xalimjonov 2/16/22 12:27 PM

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/admin")
public class AuthenticationController {

    @GetMapping()
    public String homePage() {
        return "/user/index";
    }
}
